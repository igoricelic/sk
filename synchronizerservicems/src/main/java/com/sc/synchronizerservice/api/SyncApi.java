package com.sc.synchronizerservice.api;

import com.sc.synchronizerservice.domain.dto.RequestDto;
import com.sc.synchronizerservice.service.SyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class SyncApi {

    @Autowired
    SyncService syncService;

    @RequestMapping(value = "/synchronize", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    boolean synchronize(@RequestBody RequestDto requestDto, HttpServletRequest request) {
        String token = request.getHeader("X-AUTH-TOKEN");

        return syncService.syncronize(requestDto,token);
    }
    
    @RequestMapping(value = "/sync", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Boolean sync(@RequestParam(value = "state") String state, @RequestParam(value = "code") String code) {
    	System.out.println("pre poziva sync");
        return syncService.sync(state, code);
    }
    
}
