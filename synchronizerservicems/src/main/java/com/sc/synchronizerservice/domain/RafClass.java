package com.sc.synchronizerservice.domain;

import com.sc.synchronizerservice.domain.enums.ClassType;
import com.sc.synchronizerservice.domain.enums.DayInWeek;

import java.util.List;

public class RafClass {

    private String id;

    private String subject;

    private ClassType type;

    private String instructor;

    private List<String> groups;

    private DayInWeek day;

    private String time;

    private String classroom;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public ClassType getType() {
        return type;
    }

    public void setType(ClassType type) {
        this.type = type;
    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public DayInWeek getDay() {
        return day;
    }

    public void setDay(DayInWeek day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }
}
