package com.sc.synchronizerservice.domain.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class RequestDto {

    @DateTimeFormat(iso=DateTimeFormat.ISO.DATE_TIME)
    private Date startInterval;
    @DateTimeFormat(iso=DateTimeFormat.ISO.DATE_TIME)
    private Date endINterval;

    public Date getStartInterval() {
        return startInterval;
    }

    public void setStartInterval(Date startInterval) {
        this.startInterval = startInterval;
    }

    public Date getEndINterval() {
        return endINterval;
    }

    public void setEndINterval(Date endINterval) {
        this.endINterval = endINterval;
    }
}
