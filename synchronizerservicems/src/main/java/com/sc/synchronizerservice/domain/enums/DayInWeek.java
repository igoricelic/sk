package com.sc.synchronizerservice.domain.enums;

public enum DayInWeek {
	Ponedeljak,
	Utorak,
	Sreda,
	Cetvrtak,
	Petak,
	Subota,
	Nedelja;
	
	public static DayInWeek getByName (String name) {
		switch (name) {
			case " PON ":
				return DayInWeek.Ponedeljak;
			case " UTO ":
				return DayInWeek.Utorak;
			case " SRE ":
				return DayInWeek.Sreda;
			case " ČET ":
				return DayInWeek.Cetvrtak;
			case " PET ":
				return DayInWeek.Petak;
			case " SUB ":
				return DayInWeek.Subota;
			case " NED":
				return DayInWeek.Nedelja;
			default:
				break;
			}
		return null;
	}

}
