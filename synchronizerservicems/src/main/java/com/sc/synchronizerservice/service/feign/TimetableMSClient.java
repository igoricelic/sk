package com.sc.synchronizerservice.service.feign;

import com.sc.synchronizerservice.domain.RafClass;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Repository
@FeignClient(name="timetablemicroservice")
public interface TimetableMSClient {
	
    @RequestMapping(value = "/classes/scheduleByGroup", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<RafClass> scheduleByGroup (@RequestParam(name="group") String group);
}