package com.sc.synchronizerservice.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets.Details;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.Calendar.Events;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.sc.synchronizerservice.domain.RafClass;
import com.sc.synchronizerservice.domain.dto.RequestDto;
import com.sc.synchronizerservice.domain.enums.DayInWeek;
import com.sc.synchronizerservice.service.SyncService;
import com.sc.synchronizerservice.service.feign.TimetableMSClient;
import com.sc.synchronizerservice.service.feign.UserServiceMSClient;

@Service
public class SyncServiceImpl implements SyncService {

	@Autowired
	private TimetableMSClient timetableMSClient;

	@Autowired
	private UserServiceMSClient userServiceMSClient;

	// GOogle calendar
	private com.google.api.services.calendar.Calendar client;

	private HashMap<DayInWeek, List<Date>> map;

	private static final String APPLICATION_NAME = "";
	private static HttpTransport httpTransport;
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private GoogleClientSecrets clientSecrets;
	private GoogleAuthorizationCodeFlow flow;
	private Credential credential;

	@Value("${google.client.client-id}")
	private String clientId;
	@Value("${google.client.client-secret}")
	private String clientSecret;
	@Value("${google.client.redirectUri}")
	private String redirectURI;

	@Override
	public boolean syncronize(RequestDto requestDto, String token) {
		String grupa = userServiceMSClient.groupFromToken(token);
		System.out.println("grupa: " + grupa);
		List<RafClass> raspored = timetableMSClient.scheduleByGroup(grupa);
		System.out.println(raspored);

		return true;
	}

	@Override
	public Boolean sync(String state, String code) {
//		final DateTime date1 = new DateTime("2018-05-05T16:30:00.000+05:30");
//		final DateTime date2 = new DateTime(new Date());
//		com.google.api.services.calendar.model.Events eventList;
//		System.out.println("Code: "+code);
//		String message;
		try {
			if (flow == null) {
				Details web = new Details();
				web.setClientId(clientId);
				web.setClientSecret(clientSecret);
				clientSecrets = new GoogleClientSecrets().setWeb(web);
				httpTransport = GoogleNetHttpTransport.newTrustedTransport();
				flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY, clientSecrets,
						Collections.singleton(CalendarScopes.CALENDAR)).build();
			}
			TokenResponse response = flow.newTokenRequest(code).setRedirectUri(redirectURI).execute();
			credential = flow.createAndStoreCredential(response, "userID");
			client = new com.google.api.services.calendar.Calendar.Builder(httpTransport, JSON_FACTORY, credential)
					.setApplicationName(APPLICATION_NAME).build();
			
			// primer: 02-01-1996x06-06-2018x306
			String requestCompoundArr[] = state.split("x");
			String startIntervalString = requestCompoundArr[0];
			String endIntervalString = requestCompoundArr[1];
			String grupa = requestCompoundArr[2];
			System.out.println("start:"+startIntervalString+
								"end:"+endIntervalString+
								"grupa:"+grupa);
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Date startIntervalParsed = new Date();
			startIntervalParsed = simpleDateFormat.parse(startIntervalString);
			Date endIntervalParsed = new Date();
			endIntervalParsed = simpleDateFormat.parse(endIntervalString);
			
			
			System.out.println("datum S =>"+startIntervalParsed);
			System.out.println("datum E=>"+endIntervalParsed);
			map = new HashMap<DayInWeek, List<Date>>();
			initMap(startIntervalParsed, endIntervalParsed);
			List<RafClass> raspored = timetableMSClient.scheduleByGroup(grupa);
			//kreiranje liste google eventova
			List<Event> finalEventsForGoogleCalendar = new ArrayList<Event>();
			for(RafClass rafClass: raspored) {
				List<Date> dates = map.get(rafClass.getDay());
				
				for(Date d: dates) {
					Event event = new Event();
					event.setSummary(rafClass.getSubject());
//					event.setDescription();
					
					String[] time = rafClass.getTime().split("-");
					String[] startHoursAndMins = time[0].split(":");
					String[] endHoursAndMins = time[1].split(":");
					
					EventDateTime eventDateTimeStart = new EventDateTime();
					d.setHours(Integer.parseInt(startHoursAndMins[0]));
					d.setMinutes(Integer.parseInt(startHoursAndMins[1]));
					DateTime dateTimeStart = new DateTime(d);
					eventDateTimeStart.setDateTime(dateTimeStart);
					event.setStart(eventDateTimeStart);
					
					
					EventDateTime eventDateTimeEnd = new EventDateTime();
					d.setHours(Integer.parseInt(endHoursAndMins[0]));
//					d.setMinutes(Integer.parseInt(endHoursAndMins[1]));
					DateTime dateTimeEnd = new DateTime(d);
					eventDateTimeEnd.setDateTime(dateTimeEnd);
					event.setEnd(eventDateTimeEnd);
					
					finalEventsForGoogleCalendar.add(event);
				}
			}
			
			
			
			for(Event event: finalEventsForGoogleCalendar) {
				client.events().insert("primary", event).execute();
				System.out.println("The event is saved!");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}

	private void initMap(Date startInterval, Date endInterval) {
		List<Date> dates = getDaysBetweenDates(startInterval, endInterval);
		Calendar calendar = Calendar.getInstance();
		for (Date d : dates) {
			calendar.setTime(d);
			int brojDana = calendar.get(Calendar.DAY_OF_WEEK);
			DayInWeek dayInWeek = getDanZaBroj(brojDana);
			List<Date> list = map.get(dayInWeek);
			if (list == null) {
				list = new ArrayList<Date>();
				map.put(dayInWeek, list);
			}
			list.add(d);
		}

	}

	public DayInWeek getDanZaBroj(int brojDana) {
		switch (brojDana) {
		case 1:
			return DayInWeek.Ponedeljak;
		case 2:
			return DayInWeek.Utorak;
		case 3:
			return DayInWeek.Sreda;
		case 4:
			return DayInWeek.Cetvrtak;
		case 5:
			return DayInWeek.Petak;
		case 6:
			return DayInWeek.Subota;
		case 7:
			return DayInWeek.Nedelja;
		default:
			return null;
		}
	}

	private List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
		List<Date> dates = new ArrayList<Date>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (calendar.getTime().before(enddate)) {
			Date result = calendar.getTime();
			dates.add(result);
			calendar.add(Calendar.DATE, 1);
		}
		return dates;
	}
}
