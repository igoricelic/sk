package com.sc.synchronizerservice.service;

import com.sc.synchronizerservice.domain.dto.RequestDto;
import org.springframework.stereotype.Repository;

@Repository
public interface SyncService {

    public boolean syncronize(RequestDto requestDto,String token);
    
    public Boolean sync(String state, String code);
}
