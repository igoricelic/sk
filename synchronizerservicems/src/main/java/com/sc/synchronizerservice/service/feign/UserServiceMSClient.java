package com.sc.synchronizerservice.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Repository
@FeignClient(name="userservicems")
public interface UserServiceMSClient {

    @RequestMapping(value = "/groupFromToken", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String groupFromToken(@RequestParam(name="token") String token);
    
}
