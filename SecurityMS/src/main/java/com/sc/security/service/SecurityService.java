package com.sc.security.service;

import com.sc.security.domain.HttpServletRequestDto;
import com.sc.security.domain.RolePermission;

public interface SecurityService {
	
	public Boolean permit (HttpServletRequestDto requestDto);
	
	public Iterable<RolePermission> findAll ();

}
