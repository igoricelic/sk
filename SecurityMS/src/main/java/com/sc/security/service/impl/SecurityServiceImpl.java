package com.sc.security.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sc.security.dao.SecurityDao;
import com.sc.security.domain.HttpServletRequestDto;
import com.sc.security.domain.RolePermission;
import com.sc.security.service.SecurityService;
import com.sc.security.service.TokenService;
import com.sc.security.service.feign.UserServiceMSClient;

@Service
public class SecurityServiceImpl implements SecurityService {
	
	@Autowired
	private SecurityDao securityDao;
	
	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private UserServiceMSClient userServiceClient;

	@Override
	public Boolean permit(HttpServletRequestDto requestDto) {
		if(requestDto.getMethod()==null || requestDto.getUri()==null) return false;
		RolePermission rolePermission = securityDao.findByMethodAndUri(requestDto.getMethod(), requestDto.getUri());
		if(rolePermission==null) return false;
		System.out.println(rolePermission.toString());
		long role = 0;
		if(requestDto.getToken()!=null && !requestDto.getToken().trim().equals("")) {
			String email = tokenService.getEmailForToken(requestDto.getToken());
			role = userServiceClient.getRoleForEmail(email);
		}
		return (rolePermission.getMinRole()<=role);
	}

	@Override
	public Iterable<RolePermission> findAll() {
		return securityDao.findAll();
	}

}
