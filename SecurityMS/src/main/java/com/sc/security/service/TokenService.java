package com.sc.security.service;

public interface TokenService {
	
	public String getTokenForEmail (String email);
	
	public String getEmailForToken (String token);

}
