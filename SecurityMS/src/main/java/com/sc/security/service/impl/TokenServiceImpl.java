package com.sc.security.service.impl;

import org.springframework.stereotype.Service;

import com.sc.security.service.TokenService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenServiceImpl implements TokenService {
	private static final String TOKEN_SECRET_KEY = "PoPoKaTePeTl";

	@Override
	public String getTokenForEmail(String email) {
		return Jwts.builder().setSubject(email).signWith(SignatureAlgorithm.HS512, TOKEN_SECRET_KEY).compact();
	}

	@Override
	public String getEmailForToken(String token) {
		Claims userToken = Jwts.parser().setSigningKey(TOKEN_SECRET_KEY).parseClaimsJws(token).getBody();
		return userToken.getSubject();
	}

}
