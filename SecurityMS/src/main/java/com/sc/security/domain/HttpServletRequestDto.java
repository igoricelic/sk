package com.sc.security.domain;

public class HttpServletRequestDto {
	
	private String token;
	
	private String uri;
	
	private String method;
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getUri() {
		return uri;
	}
	
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public String getMethod() {
		return method;
	}
	
	public void setMethod(String method) {
		this.method = method;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(20);
		sb.append("Token: ").append(token)
		.append(", Uri: ").append(uri)
		.append(", Method: ").append(method);
		return sb.toString();
	}

}
