package com.sc.security.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Table(name="role_permission")
@Entity
@JsonIgnoreProperties(ignoreUnknown=true)
public class RolePermission {
	
	@Id 
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="min_role")
	private Long minRole;
	
	@Column(name="uri")
	private String uri;
	
	@Column(name="method")
	private String method;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getMinRole() {
		return minRole;
	}
	
	public void setMinRole(Long minRole) {
		this.minRole = minRole;
	}
	
	public String getUri() {
		return uri;
	}
	
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public String getMethod() {
		return method;
	}
	
	public void setMethod(String method) {
		this.method = method;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(20);
		sb.append("Min role: ").append(minRole)
		.append(", Uri: ").append(uri)
		.append(", Method: ").append(method);
		return sb.toString();
	}

}
