package com.sc.security.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.sc.security.domain.RolePermission;

@RepositoryRestResource(collectionResourceRel = "role_permission", path = "role_permission")
public interface SecurityDao extends CrudRepository<RolePermission, Long> {
	
	public RolePermission findByMethodAndUri (@Param("method") String method, @Param("uri") String uri);

}
