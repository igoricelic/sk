package com.sc.security.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sc.security.domain.HttpServletRequestDto;
import com.sc.security.domain.RolePermission;
import com.sc.security.service.SecurityService;
import com.sc.security.service.TokenService;

@RestController
public class SecurityApi {
	
	@Autowired
	private SecurityService securityService;
	
	@Autowired
	private TokenService tokenService;
	
	@RequestMapping(value = "/permit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Boolean permit(@RequestBody HttpServletRequestDto requestDto) {
		System.out.println("Stigao");
		System.out.println(requestDto);
		return securityService.permit(requestDto);
	}
	
	@RequestMapping(value = "/createToken", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String createToken(@RequestBody String email) {
		System.out.println("stigao zahtev");
		System.out.println(email.toString());
		return tokenService.getTokenForEmail(email.toString());
	}
	
	@RequestMapping(value = "/findAllRolePermissions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Iterable<RolePermission> findAllRolePermissions() {
		return securityService.findAll();
	}
	
	@RequestMapping(value = "/getEmailFromToken", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String getEmailFromToken (String token) {
		return tokenService.getEmailForToken(token);
	}

}
