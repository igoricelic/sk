package com.sc.userservice.dao;

import com.sc.userservice.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User,String> {

    public User findByEmailAndPassword(String email,String password);

    public User findByEmail(String email);

}
