package com.sc.userservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document(collection = "users")
@JsonIgnoreProperties
public class User {

    @Id
    private String id;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private int role;
    private String index;
    private String grupa;

    public User(){}

    public User(String password, String firstName, String lastName, String email, int role, String index, String grupa) {
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.role = role;
        this.index = index;
        this.grupa = grupa;
    }



    public void setId(String id) {
        this.id = id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public void setGrupa(String grupa) {
        this.grupa = grupa;
    }

    public String getId() {

        return id;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public int getRole() {
        return role;
    }

    public String getIndex() {
        return index;
    }

    public String getGrupa() {
        return grupa;
    }
}
