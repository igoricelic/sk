package com.sc.userservice.domain.dto;

public class RegisterDto {

    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String index;
    private String grupa;

    public RegisterDto() {
    }

    public RegisterDto(String password, String firstName, String lastName, String email, String index, String grupa) {
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.index = index;
        this.grupa = grupa;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public void setGrupa(String grupa) {
        this.grupa = grupa;
    }

    public String getPassword() {

        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getIndex() {
        return index;
    }

    public String getGrupa() {
        return grupa;
    }
}
