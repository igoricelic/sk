package com.sc.userservice.domain.dto;

import com.sc.userservice.domain.User;

public class LoginResponseDto {

    private User user;
    private String token;
    private boolean uspjeh;

    public LoginResponseDto() {
    }

    public LoginResponseDto(User user, String token, boolean uspjeh) {
        this.user = user;
        this.token = token;
        this.uspjeh = uspjeh;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setUspjeh(boolean uspjeh) {
        this.uspjeh = uspjeh;
    }

    public User getUser() {

        return user;
    }

    public String getToken() {
        return token;
    }

    public boolean isUspjeh() {
        return uspjeh;
    }
}
