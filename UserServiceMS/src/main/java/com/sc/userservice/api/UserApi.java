package com.sc.userservice.api;

import com.sc.userservice.domain.dto.LoginDto;
import com.sc.userservice.domain.dto.LoginResponseDto;
import com.sc.userservice.domain.dto.RegisterDto;
import com.sc.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
public class UserApi {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Boolean deleteContent(Long value) {
		System.out.println("Value: "+value);
		System.out.println("It works!!!");
		return true;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody LoginResponseDto login(@RequestBody LoginDto loginDto) {
		return userService.login(loginDto.getEmail(),loginDto.getPassword());
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Boolean register(@RequestBody RegisterDto registerDto) {
		return userService.register(registerDto);
	}

	@RequestMapping(value = "/roleForEmail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody int getRoleForEmail(String email) {
		return userService.getRoleForEmail(email);
	}
	
	@RequestMapping(value = "/groupFromToken", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String groupFromToken(String token) {
		return userService.groupFromToken(token);
	}

}
