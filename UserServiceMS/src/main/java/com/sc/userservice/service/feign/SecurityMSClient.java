package com.sc.userservice.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

@Repository
@FeignClient(name="securityms")
public interface SecurityMSClient {

    @RequestMapping(value="/createToken", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String createToken (@RequestBody String email);

    @RequestMapping(value="/getEmailFromToken", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String getEmailFromToken (@RequestParam(name="token") String token);
    
}

