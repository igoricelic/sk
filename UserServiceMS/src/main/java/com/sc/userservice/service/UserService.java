package com.sc.userservice.service;

import com.sc.userservice.domain.dto.LoginResponseDto;
import com.sc.userservice.domain.dto.RegisterDto;

public interface UserService {

    public LoginResponseDto login(String email, String password);

    public boolean register(RegisterDto registerDto);

    int getRoleForEmail(String email);
    
    public String groupFromToken (String token);
}
