package com.sc.userservice.service.impl;

import com.sc.userservice.dao.UserDao;
import com.sc.userservice.domain.User;
import com.sc.userservice.domain.dto.LoginResponseDto;
import com.sc.userservice.domain.dto.RegisterDto;
import com.sc.userservice.service.UserService;
import com.sc.userservice.service.feign.SecurityMSClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private SecurityMSClient securityMSClient;

    @Override
    public LoginResponseDto login(String email,String password) {
        User user = userDao.findByEmailAndPassword(email,password);

        if(user == null){
            return new LoginResponseDto(user,null,false);
        }
        String token = securityMSClient.createToken(email);
        return new LoginResponseDto(user,token,true);
    }

    @Override
    public boolean register(RegisterDto registerDto){
        User user = new User(registerDto.getPassword(),registerDto.getFirstName(),registerDto.getLastName(),
                             registerDto.getEmail(),1,registerDto.getIndex(),registerDto.getGrupa());
        user = userDao.save(user);
        if(user == null) return false;
        return true;
    }

    @Override
    public int getRoleForEmail(String email) {
        User user = userDao.findByEmail(email);
        if(user == null) return 0;
        return user.getRole();
    }

	@Override
	public String groupFromToken(String token) {
        System.out.println(token);
		String email = securityMSClient.getEmailFromToken(token);
        System.out.println(email);
		User user = userDao.findByEmail(email);
        System.out.println(user);
		return (user==null) ? null : user.getGrupa();
	}
}
