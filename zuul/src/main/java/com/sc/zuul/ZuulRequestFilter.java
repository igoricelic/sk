package com.sc.zuul;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.sc.zuul.domain.HttpServletRequestDto;
import com.sc.zuul.service.SecurityMSClient;

@Component
public class ZuulRequestFilter extends ZuulFilter {
	private static final String TOKEN_NAME_IN_HEAD = "X-AUTH-TOKEN";
	
	@Autowired
	private SecurityMSClient securityClient;

	@Override
	public Object run() throws ZuulException {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();

		HttpServletRequestDto requestDto = new HttpServletRequestDto();
		requestDto.setToken(request.getHeader(TOKEN_NAME_IN_HEAD));
		requestDto.setMethod(request.getMethod());
		requestDto.setUri(request.getRequestURI());
		System.out.println(requestDto.toString());
		try {
			Boolean permit = securityClient.permit(requestDto);
			System.out.println(permit);
			if (!permit) {
				ctx.setResponseStatusCode(401);
				ctx.setSendZuulResponse(false);
			}
		} catch (Exception e) {
			e.printStackTrace();

			ctx.setResponseStatusCode(401);
			ctx.setSendZuulResponse(false);
		}

		return null;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public String filterType() {
		return "pre";
	}

}
