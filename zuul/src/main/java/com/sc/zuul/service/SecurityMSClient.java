package com.sc.zuul.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sc.zuul.domain.HttpServletRequestDto;

@Repository
@FeignClient(name="securityms")
public interface SecurityMSClient {
	
	@RequestMapping(value="/permit", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Boolean permit (@RequestBody HttpServletRequestDto requestDto);

}
