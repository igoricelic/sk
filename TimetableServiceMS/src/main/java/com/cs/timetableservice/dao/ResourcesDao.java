package com.cs.timetableservice.dao;

import org.springframework.data.repository.CrudRepository;

import com.cs.timetableservice.domain.Resources;

public interface ResourcesDao extends CrudRepository<Resources, String> {

}
