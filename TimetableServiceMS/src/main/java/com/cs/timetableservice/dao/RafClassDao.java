package com.cs.timetableservice.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cs.timetableservice.domain.RafClass;

public interface RafClassDao extends CrudRepository<RafClass, String> {
	
	public List<RafClass> findByGroupsIn (String group);

}
