package com.cs.timetableservice.domain.dto;

import java.util.List;
import java.util.Set;

import com.cs.timetableservice.domain.RafClass;
import com.cs.timetableservice.domain.Resources;

public class ParsingDto {
	
	private Resources resources;
	
	private List<RafClass> classes;
	
	public ParsingDto(Resources resources, List<RafClass> classes) {
		this.resources = resources;
		this.classes = classes;
	}
	
	public Resources getResources() {
		return resources;
	}
	
	public List<RafClass> getClasses() {
		return classes;
	}

}
