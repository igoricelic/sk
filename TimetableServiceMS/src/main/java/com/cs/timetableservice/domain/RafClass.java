package com.cs.timetableservice.domain;

import java.util.List;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import com.cs.timetableservice.domain.enums.ClassType;
import com.cs.timetableservice.domain.enums.DayInWeek;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Document(collection = "classes")
@JsonIgnoreProperties
public class RafClass {
	@Id
	private String id;
	
	private String subject;
	
	private ClassType type;
	
	private String instructor;
	
	private List<String> groups;
	
	private DayInWeek day;
	
	private String time;
	
	private String classroom;

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public ClassType getType() {
		return type;
	}

	public void setType(ClassType type) {
		this.type = type;
	}

	public String getInstructor() {
		return instructor;
	}

	public void setInstructor(String instructor) {
		this.instructor = instructor;
	}
	
	public List<String> getGroups() {
		return groups;
	}
	
	public void setGroups(List<String> groups) {
		this.groups = groups;
	}

	public DayInWeek getDay() {
		return day;
	}

	public void setDay(DayInWeek day) {
		this.day = day;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getClassroom() {
		return classroom;
	}

	public void setClassroom(String classroom) {
		this.classroom = classroom;
	}
	
	@Override
	public String toString() {
		return "RafClass [id=" + id + ", subject=" + subject + ", type=" + type + ", instructor=" + instructor
				+ ", groups=" + groups + ", day=" + day + ", time=" + time + ", classroom=" + classroom + "]";
	}

	public static class RafClassBuilder {
		private String subject;
		private ClassType type;
		private String instructor;
		private List<String> groups;
		private DayInWeek day;
		private String time;
		private String classroom;
		
		public RafClassBuilder setSubject (String subject) {
			this.subject = subject;
			return this;
		}
		
		public RafClassBuilder setType(ClassType type) {
			this.type = type;
			return this;
		}
		
		public RafClassBuilder setInstructor(String instructor) {
			this.instructor = instructor;
			return this;
		}
		
		public RafClassBuilder setGroups(List<String> groups) {
			this.groups = groups;
			return this;
		}
		
		public RafClassBuilder setDay(DayInWeek day) {
			this.day = day;
			return this;
		}
		
		public RafClassBuilder setTime(String time) {
			this.time = time;
			return this;
		}
		
		public RafClassBuilder setClassroom(String classroom) {
			this.classroom = classroom;
			return this;
		}
		
		public RafClass build () {
			RafClass rafClass = new RafClass();
			rafClass.setSubject(subject);
			rafClass.setType(type);
			rafClass.setInstructor(instructor);
			rafClass.setGroups(groups);
			rafClass.setDay(day);
			rafClass.setTime(time);
			rafClass.setClassroom(classroom);
			return rafClass;
		}
		
	}

}
