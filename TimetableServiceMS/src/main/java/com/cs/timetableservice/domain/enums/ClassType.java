package com.cs.timetableservice.domain.enums;

public enum ClassType {
	PREDAVANJA,
	VEZBE,
	LABORATORIJSKE_VEZBE,
	PREDAVANJA_I_VEZBE;
	
	public static ClassType getByName (String name) {
		switch (name) {
			case "Predavanja":
				return ClassType.PREDAVANJA;
			case "Vezbe":
				return ClassType.VEZBE;
			case "Laboratorijske vezbe":
				return ClassType.LABORATORIJSKE_VEZBE;
			case "Predavanja i vezbe":
				return ClassType.PREDAVANJA_I_VEZBE;
			default:
				break;
		}
		return null;
	}

}
