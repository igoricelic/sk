package com.cs.timetableservice.domain;

import java.util.Set;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Document(collection = "resources")
@JsonIgnoreProperties
public class Resources {
	
	@Id
	private String id;
	
	private Set<String> instructors;
	
	private Set<String> subjects;
	
	private Set<String> groups;
	
	private Set<String> classRooms;
	
	public Resources () {}
	
	public Resources (Set<String> instructors, Set<String> subjects, Set<String> groups,
			Set<String> classRooms) {
		this.instructors = instructors;
		this.subjects = subjects;
		this.groups = groups;
		this.classRooms = classRooms;
	}

	public Set<String> getInstructors() {
		return instructors;
	}

	public void setInstructors(Set<String> instructors) {
		this.instructors = instructors;
	}

	public Set<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(Set<String> subjects) {
		this.subjects = subjects;
	}

	public Set<String> getGroups() {
		return groups;
	}

	public void setGroups(Set<String> groups) {
		this.groups = groups;
	}

	public Set<String> getClassRooms() {
		return classRooms;
	}

	public void setClassRooms(Set<String> classRooms) {
		this.classRooms = classRooms;
	}

}
