package com.cs.timetableservice.domain.enums;

public enum DayInWeek {
	Ponedeljak,
	Utorak,
	Sreda,
	Cetvrtak,
	Petak,
	Subota,
	Nedelja;
	
	public static DayInWeek getByName (String name) {
		if(name.contains("PON")) {
			return DayInWeek.Ponedeljak;
		} else if(name.contains("UTO")) {
			return DayInWeek.Utorak;
		} else if(name.contains("SRE")) {
			return DayInWeek.Sreda;
		} else if(name.contains("ČET")) {
			return DayInWeek.Cetvrtak;
		} else if(name.contains("PET")) {
			return DayInWeek.Petak;
		} else if(name.contains("SUB")) {
			return DayInWeek.Subota;
		} else if(name.contains("NED")) {
			return DayInWeek.Nedelja;
		}
		return null;
	}

}
