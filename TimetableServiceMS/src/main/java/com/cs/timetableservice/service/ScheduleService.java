package com.cs.timetableservice.service;

import java.util.List;

import com.cs.timetableservice.domain.RafClass;
import com.cs.timetableservice.domain.Resources;
import com.cs.timetableservice.domain.dto.ParsingDto;

public interface ScheduleService {
	
	public List<RafClass> mySchedule (String token);
	
	public Iterable<RafClass> fullSchedule ();
	
	public List<RafClass> scheduleByGroup (String group);
	
	public ParsingDto fullData ();
	
	public Resources getResources ();

}
