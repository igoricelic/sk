package com.cs.timetableservice.service.impl;

public enum CSVHeaderFormat {
	SUBJECT,
	TYPE,
	INSTRUCTOR,
	GROUPS,
	DAY,
	TIME,
	CLASSROOM;

}
