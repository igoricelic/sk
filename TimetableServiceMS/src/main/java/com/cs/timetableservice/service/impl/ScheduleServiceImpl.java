package com.cs.timetableservice.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cs.timetableservice.dao.RafClassDao;
import com.cs.timetableservice.dao.ResourcesDao;
import com.cs.timetableservice.domain.RafClass;
import com.cs.timetableservice.domain.Resources;
import com.cs.timetableservice.domain.dto.ParsingDto;
import com.cs.timetableservice.service.ScheduleService;
import com.cs.timetableservice.service.feign.UserServiceMSClient;

@Service
public class ScheduleServiceImpl implements ScheduleService {
	
	@Autowired
	private RafClassDao rafClassDao;
	
	@Autowired
	private ResourcesDao resourcesDao;
	
	@Autowired
	private UserServiceMSClient userServiceMS;

	@Override
	public List<RafClass> mySchedule(String token) {
		String group = userServiceMS.groupFromToken(token);
		if(group==null) return null;
		return rafClassDao.findByGroupsIn(group);
	}

	@Override
	public Iterable<RafClass> fullSchedule() {
		return rafClassDao.findAll();
	}

	@Override
	public ParsingDto fullData() {
		Resources resources = resourcesDao.findAll().iterator().next();
		List<RafClass> classes = new ArrayList<>();
		rafClassDao.findAll().forEach(classes::add);
		return new ParsingDto(resources, classes);
	}

	@Override
	public List<RafClass> scheduleByGroup(String group) {
		return rafClassDao.findByGroupsIn(group);
	}

	@Override
	public Resources getResources() {
		Resources resources = resourcesDao.findAll().iterator().next();
		return resources;
	}

}
