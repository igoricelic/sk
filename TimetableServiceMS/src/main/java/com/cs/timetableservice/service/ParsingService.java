package com.cs.timetableservice.service;

import org.springframework.web.multipart.MultipartFile;

import com.cs.timetableservice.domain.dto.ParsingDto;
import com.cs.timetableservice.exceptions.ParsingException;

public interface ParsingService {
	
	public ParsingDto parseFile (MultipartFile file) throws ParsingException;

}
