package com.cs.timetableservice.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cs.timetableservice.dao.RafClassDao;
import com.cs.timetableservice.dao.ResourcesDao;
import com.cs.timetableservice.domain.RafClass;
import com.cs.timetableservice.domain.Resources;
import com.cs.timetableservice.domain.dto.ParsingDto;
import com.cs.timetableservice.exceptions.ParsingException;
import com.cs.timetableservice.service.ParsingService;
import com.cs.timetableservice.service.UploadService;

@Service
public class UploadServiceImpl implements UploadService {

	@Autowired
	private ParsingService parsingService;
	
	@Autowired
	private RafClassDao rafClassDao;
	
	@Autowired
	private ResourcesDao resourcesDao;
	
	@Override
	public Boolean uploadFile (MultipartFile file) throws ParsingException {
		ParsingDto parsingDto = parsingService.parseFile(file);
		List<RafClass> classes = parsingDto.getClasses();
		Resources resources = parsingDto.getResources();
		rafClassDao.deleteAll();
		rafClassDao.saveAll(classes);
		resourcesDao.deleteAll();
		resourcesDao.save(resources);
		return true;
	}

}
