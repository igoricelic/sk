package com.cs.timetableservice.service.impl;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cs.timetableservice.domain.RafClass;
import com.cs.timetableservice.domain.RafClass.RafClassBuilder;
import com.cs.timetableservice.domain.Resources;
import com.cs.timetableservice.domain.dto.ParsingDto;
import com.cs.timetableservice.domain.enums.ClassType;
import com.cs.timetableservice.domain.enums.DayInWeek;
import com.cs.timetableservice.exceptions.ParsingException;
import com.cs.timetableservice.service.ParsingService;


@Service
public class ParsingServiceImpl implements ParsingService {

	@Override
	public ParsingDto parseFile(MultipartFile file) throws ParsingException {
		String extension = FilenameUtils.getExtension(file.getOriginalFilename());

		switch (extension) {
			case "json":
				return parseJSONFile(file);
			case "xml":
				return parseXMLFile(file);
			case "csv":
				return parseCSVFile(file);
			default:
				break;
		}
		
		return null;
	}
	
	public ParsingDto parseCSVFile(MultipartFile file) throws ParsingException {
		CSVParser parser = null;
		Set<String> instructors = new HashSet<>();
		Set<String> subjects = new HashSet<>();
		Set<String> groups = new HashSet<>();
		List<String> lGroups = null;
		Set<String> classRooms = new HashSet<>();
		try {
			Reader reader = new InputStreamReader(file.getInputStream());
			parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader(CSVHeaderFormat.class));
			lGroups = null;
			List<CSVRecord> records = parser.getRecords();
			List<RafClass> rafClasses = new LinkedList<>();
			
			for(CSVRecord record: records) {
				instructors.add(record.get(CSVHeaderFormat.INSTRUCTOR).trim());
				subjects.add(record.get(CSVHeaderFormat.SUBJECT).trim());
				classRooms.add(record.get(CSVHeaderFormat.CLASSROOM).trim());
				lGroups = getGroupsFromString(record.get(CSVHeaderFormat.GROUPS).trim());
				groups.addAll(lGroups);
				rafClasses.add(
				new RafClassBuilder().setSubject(record.get(CSVHeaderFormat.SUBJECT).trim())
					.setType(ClassType.getByName(record.get(CSVHeaderFormat.TYPE).trim()))
					.setInstructor(record.get(CSVHeaderFormat.INSTRUCTOR).trim())
					.setGroups(lGroups)
					.setDay(DayInWeek.getByName(record.get(CSVHeaderFormat.DAY).trim()))
					.setTime(record.get(CSVHeaderFormat.TIME).trim())
					.setClassroom(record.get(CSVHeaderFormat.CLASSROOM).trim()).build()
				);
			}
			
			return new ParsingDto(new Resources(instructors, subjects, groups, classRooms), rafClasses);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ParsingDto parseJSONFile(MultipartFile file) throws ParsingException {
		return null;
	}
	
	public ParsingDto parseXMLFile(MultipartFile file) throws ParsingException {
		return null;
	}
	
	private List<String> getGroupsFromString (String groupsToken) {
		List<String> groups = new LinkedList<>();
		String[] tokens = groupsToken.trim().split(",");
		for(String token: tokens) {
			groups.add(token.trim());
		}
		return groups;
	}

}
