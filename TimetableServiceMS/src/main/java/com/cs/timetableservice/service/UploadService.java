package com.cs.timetableservice.service;

import org.springframework.web.multipart.MultipartFile;

import com.cs.timetableservice.exceptions.ParsingException;

public interface UploadService {
	
	public Boolean uploadFile (MultipartFile file) throws ParsingException;

}
