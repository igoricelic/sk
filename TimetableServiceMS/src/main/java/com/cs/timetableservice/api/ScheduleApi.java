package com.cs.timetableservice.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cs.timetableservice.ApplicationConstants;
import com.cs.timetableservice.domain.RafClass;
import com.cs.timetableservice.domain.Resources;
import com.cs.timetableservice.domain.dto.ParsingDto;
import com.cs.timetableservice.service.ScheduleService;

@RestController
@RequestMapping(value="/classes")
public class ScheduleApi {
	
	@Autowired
	private ScheduleService scheduleService;
	
	@RequestMapping(value = "/mySchedule", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<RafClass> mySchedule (HttpServletRequest request) {
		return scheduleService.mySchedule(request.getHeader(ApplicationConstants.TOKEN_NAME_IN_HEAD));
	}
	
	@RequestMapping(value = "/resources", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Resources resources () {
		return scheduleService.getResources();
	}
	
	@RequestMapping(value = "/scheduleByGroup", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<RafClass> scheduleByGroup (String group) {
		return scheduleService.scheduleByGroup(group);
	}
	
	@RequestMapping(value = "/fullSchedule", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Iterable<RafClass> fullSchedule (HttpServletRequest request) {
		return scheduleService.fullSchedule();
	}
	
	@RequestMapping(value = "/fullData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ParsingDto fullData () {
		return scheduleService.fullData();
	}

}
