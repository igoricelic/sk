package com.cs.timetableservice.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.cs.timetableservice.exceptions.ParsingException;
import com.cs.timetableservice.service.UploadService;

@RestController
public class UploadApi {
	
	@Autowired
	private UploadService uploadService;
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Boolean permit(@RequestParam(name="file") MultipartFile file) throws ParsingException {
		return uploadService.uploadFile(file);
	}

}
