angular.module('myApp')
    .service('HttpService',['$http','$location','$q','$localStorage',function($http,$location,$q,$localStorage){

        var backendadress = 'http://localhost:9000/';
        return {
            get:gethttp,
            post:posthttp,
            put:puthttp,
            delete:deletehttp
        };
        function gethttp(adress, params) {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: backendadress+adress,
                params:params
            }).then(function successCallback(response) {
                defer.resolve(response);
            }, function errorCallback(response) {
                defer.reject(response);
            });

            return defer.promise;
        }
        function posthttp(adress, params) {

            var defer = $q.defer();
            $http({
                method: 'POST',
                url: backendadress+adress,
                data:params
            }).then(function successCallback(response) {
                defer.resolve(response);
            }, function errorCallback(response) {
                defer.reject(response);
            });

            return defer.promise;
        }
        function puthttp(adress, port, params) {

            var defer = $q.defer();
            $http({
                method: 'PUT',
                url: backendadress+port+adress,
                data:params
            }).then(function successCallback(response) {
                defer.resolve(response);
            }, function errorCallback(response) {
                defer.reject(response);
            });

            return defer.promise;
        }
        function deletehttp(adress, port, params) {

            var defer = $q.defer();
            $http({
                method: 'DELETE',
                url: backendadress+port+adress,
                data:params
            }).then(function successCallback(response) {
                defer.resolve(response);
            }, function errorCallback(response) {
                defer.reject(response);
            });

            return defer.promise;
        }

    }]);