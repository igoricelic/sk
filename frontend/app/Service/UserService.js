angular.module('myApp')
    .service('UserService',['HttpService','$location','$q','$localStorage',function(HttpService,$location,$q,$localStorage) {

        return {
            loginUser: loginUser,
            registerUser: registrerUser
        };

        function loginUser (loginDto) {
            var adress = "userservicems/login";
            var defer = $q.defer();
            HttpService.post(adress, loginDto).then(
                function successCallback(response) {
                    defer.resolve(response);
                }, function errorCallback(response) {
                    defer.reject(response);
                });
            return defer.promise;
        }

        function registrerUser (registerDto) {
            var adress = "userservicems/register";
            var defer = $q.defer();
            HttpService.post(adress, registerDto).then(
                function successCallback(response) {
                    defer.resolve(response);
                }, function errorCallback(response) {
                    defer.reject(response);
                });
            return defer.promise;
        }

    }]);