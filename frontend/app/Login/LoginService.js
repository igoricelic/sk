angular.module('myApp')
    .service('LoginService',['HttpService','$location','$q','$localStorage',function(HttpService,$location,$q,$localStorage) {
        return {
            registration: registration,
            login: login
        };

        function login(loginDto) {
            var adress = "/login";
            var defer = $q.defer();
            HttpService.post(adress, loginDto).then(
                function successCallback(response) {
                    defer.resolve(response);
                }, function errorCallback(response) {
                    defer.reject(response);
                });
            return defer.promise;
        }

        function registration(registrationDto) {
            var adress = "/register";
            var defer = $q.defer();
            HttpService.post(adress, registrationDto).then(
                function successCallback(response) {
                    defer.resolve(response);
                }, function errorCallback(response) {
                    defer.reject(response);
                });
            return defer.promise;
        };

    }]);