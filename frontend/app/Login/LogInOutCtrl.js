'use strict';

angular.module('myApp')
    .controller('LoginCtrl', ['$rootScope','$scope', '$location', 'UserService', 'LoginService', '$localStorage', 'LoggedUserService', 'ActivityForUser', 'AllUsersService',function ($rootScope, $scope, $location, UserService, LoginService, $localStorage, LoggedUserService, ActivityForUser, AllUsersService) {
        if($rootScope.isLogged === true){
            $location.path('/');
        }

        // Deo vezan za prijavu usera

        $scope.email = '';
        $scope.password = '';

        $scope.emptyFieldsLogIn = false;
        $scope.wrongFill = false;
        $scope.notVerify = false;

        $scope.loginAction = function () {
            if($scope.email === '' || $scope.password === '') {
                $scope.emptyFieldsLogIn = true;
            }
            else {
                $scope.emptyFieldsLogIn = false;
                // LOGIN
            }
        };

        // Deo vezan za registraciju novog usera

        $scope.emptyFieldsRegistration = false;
        $scope.passConfPassNotEquals = false;
        $scope.isregistered = false;
        $scope.isError = false;
        $scope.registrationDto = {
            firstName: "",
            lastName: "",
            email: "",
            password: "",
            confirmPassword: "",
            index: "",
            grupa: ""
        };
        $scope.loginDto = {
            email: "",
            password: ""
        };

        $scope.isEmptyField = function (object) {
            for(var key in object){
                if(object[key] === "") return true;
            }
            return false;
        };
        $scope.clearField = function (object) {
            for(var key in object){
                object[key] = "";
            }
        };

        $scope.register = function () {
            console.log('saljem');
            UserService.registerUser($scope.registrationDto).then(
                function (response) {
                    console.log('stigao');
                    console.log(response.data);
                },
                function (response) {
                    console.log('Error!');
                    $scope.isError = true;
                }
            );
            // if (!$scope.isEmptyField($scope.registrationDto)) {
            //     if($scope.registrationDto.password !== $scope.registrationDto.confirmPassword){
            //         $scope.passConfPassNotEquals = true;
            //         return;
            //     }
            //     else{
            //         $scope.passConfPassNotEquals = false;
            //     }
            //     console.log('saljem');
            //     UserService.registerUser($scope.registrationDto).then(
            //         function (response) {
            //             console.log('stigao');
            //             console.log(response.data);
            //         },
            //         function (response) {
            //             console.log('Error!');
            //             $scope.isError = true;
            //         }
            //     );
            // }else {
            //     $scope.emptyFieldsRegistration = true;
            // }
        };

        // Odjava usera sa sistema

        $scope.logout = function () {
            $rootScope.isLogged = false;
            $rootScope.status = 0;
            $location.path("/LoginView");
        };

    }]);