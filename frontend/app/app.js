/* global angular */
var app;
app = angular.module('myApp', ['ngRoute', 'ui.bootstrap'])

	.config(function ($routeProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'profile/homepage.html',
			})
			.when('/LoginView', {
				templateUrl: 'Login/LoginView.html',
				controller: 'LoginCtrl'
			})

	})
    .config(function ($httpProvider) {
        "use strict";
        $httpProvider.interceptors.push('HttpInterceptor');

    })
	.config(function ($httpProvider) {
        "use strict";
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};
    })

	.factory('LoggedUserService', function () {
		var user = {};

		function set(data) {
			user = data;
        }
        function get() {
			return user;
        }

        return {
			set: set,
			get: get
		};
    })
	.factory('UserForShow', function () {
        var user = {};

        function set(data) {
            user = data;
        }
        function get() {
            return user;
        }

        return {
            set: set,
            get: get
        };
    })
    .factory('UserForNewActivity', function () {
        var user = {};

        function set(data) {
            user = data;
        }
        function get() {
            return user;
        }

        return {
            set: set,
            get: get
        };
    })
	.factory('ActivityForUser', function () {
		var activity = [];

		function set(data) {
			activity = data;
        }
        function get(key) {
			return activity[key];
        }
        function add(key, value) {
			activity[key] = value;
        }

        return {
			set: set,
			get: get,
			add: add
		};
    })
	.factory('AllUsersService', function () {
		var data = [];

		function set(allUsers) {
			data = allUsers;
        }
        function get() {
			return data;
        }
        function userWithId(user_id) {
			for(var i=0; i<data.length; i++){
				var login = data[i];
				if(login.id === user_id){
					return login;
				}
			}
			return null;
        }

        return {
			get: get,
			set: set,
			userWithId: userWithId
		};
    })
	.factory('ActivityForTag', function () {
		var data = [];

		function set(object) {
			data = object;
        }
        function get(tag) {
			return data[tag];
        }
        function add(tag, value) {
			data[tag] = value;
        }

        return {
			set: set,
			get: get,
			add: add
		};

    });


	angular.module('myApp').directive('ngEnter', function() {
    	return function(scope, element, attrs) {
        	element.bind("keydown keypress", function(event) {
            	if(event.which === 13) {
                	scope.$apply(function(){
                    	scope.$eval(attrs.ngEnter, {'event': event});
                	});
                	event.preventDefault();
            	}
        	});
    	};
	});

	app.factory('$localStorage', ['$window', function($window) {
		return {
			get: function(key){
				var value = $window.localStorage[key];
				if(value !== 'undefined') {
					return value ? JSON.parse(value): null;
				}
				return null;
			},
			set: function(key, value){
				$window.localStorage[key] = JSON.stringify(value);
			},
			remove: function(key){
				$window.localStorage.removeItem(key);
			}
		};
	}]);
