angular.module('myApp')
    .controller('Ctrl1', ['$scope', '$location', '$rootScope', 'LoggedUserService', 'UserForShow',function ($scope, $location, $rootScope, LoggedUserService, UserForShow) {
        $scope.image = "https://i.ytimg.com/vi/MYGcrULkmXY/maxresdefault.jpg";

        if (!angular.isUndefined($rootScope.isLogged) && $rootScope.isLogged === true) {
            UserForShow.set(LoggedUserService.get());
            $location.path('/MyProfile');
        }

    }]);