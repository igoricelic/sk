angular.module('myApp')
    .service('HttpInterceptor', ['$q', '$rootScope', '$location', '$localStorage',
        function($q, $rootScope, $location, $localStorage) {

            return {
                request: function(config) {
                    var token = $localStorage.get('token');
                    if(token !== null) config.headers['X-AUTH-TOKEN'] = token;
                    return config;
                },
                requestError: function(rejection) {
                    return $q.reject(rejection);
                },
                response: function(response) {
                    return response;
                },
                responseError: function(response) {
                    return $q.reject(response);
                }
            };
        }]);
